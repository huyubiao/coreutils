Name:       coreutils
Version:    9.6
Release:    1
License:    GPL-3.0-or-later
Summary:    A set of basic GNU tools commonly used in shell scripts
Url:        https://www.gnu.org/software/coreutils/
Source0:    https://ftp.gnu.org/gnu/%{name}/%{name}-%{version}.tar.xz

# do not make coreutils-single depend on /usr/bin/coreutils
%global __requires_exclude ^%{_bindir}/coreutils$
%global user `ls -ld $USR_SCONF|awk '{print $3}'`

Patch0:    0001-disable-test-of-rwlock.patch
# uname -p/-i to display processor type
Patch1:    coreutils-8.2-uname-processortype.patch
Patch2:    bugfix-remove-usr-local-lib-from-m4.patch
Patch3:    bugfix-dummy_help2man.patch
Patch4:    skip-the-tests-that-require-selinux-if-selinux-is-di.patch 
Patch5:    backport-config-color-alias-for-ls.patch
Patch7:    coreutils-i18n.patch
Patch8:    test-skip-overlay-filesystem-because-of-no-inotify_add_watch.patch

# Upstream patches
Patch6001: backport-ls-fix-crash-with-context.patch

Patch9001:   coreutils-9.6-sw.patch


Conflicts: filesystem < 3
# To avoid clobbering installs
Provides: /bin/sh

Conflicts: %{name}-single
Obsoletes: %{name}-common < %{version}-%{release}
Provides: %{name}-common = %{version}-%{release}

%ifnarch loongarch64
BuildRequires: strace
%endif
BuildRequires: attr, autoconf, automake, gcc, hostname, texinfo
BuildRequires: gettext-devel, gmp-devel, libacl-devel, libattr-devel
BuildRequires: libcap-devel, libselinux-devel, libselinux-utils, openssl-devel tcl

Requires: ncurses, gmp

Provides: coreutils-full = %{version}-%{release}
Provides: fileutils = %{version}-%{release}
Provides: mktemp = 4:%{version}-%{release}
Provides: sh-utils = %{version}-%{release}
Provides: stat = %{version}-%{release}
Provides: textutils = %{version}-%{release}
Obsoletes: %{name} < 8.24
Provides: bundled(gnulib)
Provides: /bin/basename, /bin/cat, /bin/chgrp, /bin/chmod, /bin/chown
Provides: /bin/cp, /bin/cut, /bin/date, /bin/dd, /bin/df, /bin/echo
Provides: /bin/env, /bin/false, /bin/ln, /bin/ls, /bin/mkdir, /bin/mknod
Provides: /bin/mktemp, /bin/mv, /bin/nice, /bin/pwd, /bin/readlink
Provides: /bin/rm, /bin/rmdir, /bin/sleep, /bin/sort, /bin/stty
Provides: /bin/sync, /bin/touch, /bin/true, /bin/uname

%description
These are the GNU core utilities.  This package is the combination of
the old GNU fileutils, sh-utils, and textutils packages.

%prep
%autosetup -N
tee DIR_COLORS{,.256color,.lightbgcolor} <src/dircolors.hin >/dev/null
%autopatch -p1

(echo ">>> Fixing permissions on tests") 2>/dev/null
find tests -name '*.sh' -perm 0644 -print -exec chmod 0755 '{}' '+'
(echo "<<< done") 2>/dev/null

# FIXME: Force a newer gettext version to workaround `autoreconf -i` errors
# with coreutils 9.6 and bundled gettext 0.19.2 from gettext-common-devel.
sed -i 's/0.19.2/0.22.5/' bootstrap.conf configure.ac

%build
autoreconf -fi

if [ %user = root ]; then
    export FORCE_UNSAFE_CONFIGURE=1
fi
CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing -fpic -fsigned-char"

# add -Wno-unused-command-line-argument when building with clang
# as a workaround for stdbuf detection faliure during configuring
%if "%{?toolchain}" == "clang"
CFLAGS="$CFLAGS -Wno-unused-command-line-argument"
%endif

export CFLAGS

# make mknod work again in chroot without /proc being mounted
export ac_cv_func_lchmod="no"

%{expand:%%global optflags %{optflags} -D_GNU_SOURCE=1}
mkdir separate && \
  (cd separate && ln -s ../configure || exit 1
%configure --with-openssl \
             --cache-file=../config.cache \
             --enable-install-program=arch \
             --enable-no-install-program=kill,uptime \
             --with-tty-group \
             DEFAULT_POSIX2_VERSION=200112 alternative=199209 || :
make all %{?_smp_mflags}

# make sure that parse-datetime.{c,y} ends up in debuginfo (#1555079)
ln -v ../lib/parse-datetime.{c,y} .
 )

%install
(cd separate && make DESTDIR=$RPM_BUILD_ROOT install)

# chroot was in /usr/sbin :
mkdir -p $RPM_BUILD_ROOT/{%{_bindir},%{_sbindir}}
mv $RPM_BUILD_ROOT/{%_bindir,%_sbindir}/chroot

%find_lang %name
# Add the %%lang(xyz) ownership for the LC_TIME dirs as well...
grep LC_TIME %name.lang | cut -d'/' -f1-6 | sed -e 's/) /) %%dir /g' >>%name.lang

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/profile.d
install -p -c -m644 DIR_COLORS{,.256color,.lightbgcolor} $RPM_BUILD_ROOT%{_sysconfdir}
install -p -c -m644 build-aux/coreutils-colorls.sh $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/colorls.sh
install -p -c -m644 build-aux/coreutils-colorls.csh $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/colorls.csh
cat > $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/sm3sum.sh << EOF
alias sm3sum='cksum -a sm3' 2>/dev/null
EOF
cat > $RPM_BUILD_ROOT%{_sysconfdir}/profile.d/sm3sum.csh << EOF
alias sm3sum 'cksum -a sm3'
EOF

%check
pushd separate
%make_build check VERBOSE=yes
popd

%files -f %{name}.lang
%{_bindir}/*
%{_sbindir}/chroot
%dir %{_libexecdir}/coreutils
%{_libexecdir}/coreutils/*.so
%doc ABOUT-NLS NEWS README THANKS TODO
%license COPYING
%config(noreplace) %{_sysconfdir}/profile.d/*
%config(noreplace) %{_sysconfdir}/DIR_COLORS*

%package_help
%files help
%{_infodir}/coreutils*
%{_mandir}/man*/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 9.6-1
- update to 9.6

* Thu Nov 28 2024 huyubiao <huyubiao@huawei.com> - 9.5-7
- sync patches from community
- add backport-head-fix-overflows-in-elide_tail_bytes_pipe.patch

* Fri Nov 1 2024 liuh <liuhuan01@kylinos.cn> - 9.5-6
- backport: tail: avoid infloop with -c on /dev/zero

* Fri Oct 25 2024 zhangyaqi <zhangyaqi@kylinos.cn> - 9.5-5
- chroot,whoami: use uintmax_t for printing uids

* Wed Sep 11 2024 huyubiao <huyubiao@huawei.com> - 9.5-4
- sync patches from community
- add backport-sort-don-t-trust-st_size-on-proc-files.patch
      backport-cat-don-t-trust-st_size-on-proc-files.patch
      backport-dd-don-t-trust-st_size-on-proc-files.patch
      backport-split-don-t-trust-st_size-on-proc-files.patch
      backport-putenv-Don-t-crash-upon-out-of-memory.patch
      backport-head-off_t-not-uintmax_t-for-file-offset.patch
      backport-shuf-avoid-integer-overflow-on-huge-inputs.patch
      backport-shuf-fix-randomness-bug.patch

* Mon Sep 02 2024 Funda Wang <fundawang@yeah.net> - 9.5-3
- Avoids false warnings with GCC 14.2.1 with -flto
- rediff sw_64 patch

* Tue Aug 13 2024 huyubiao <huyubiao@huawei.com> - 9.5-2
- fix alias sm3sum not working on bash
  delete redundant backport-chmod-fix-exit-status-when-ignoring-symlinks.patch

* Mon Jul 15 2024 dillon chen <dillon.chen@gmail.com> - 9.5-1
- update to 9.5
- getgrouplist.patch: drop a patch no longer needed
- CVE-2024-0684 patch: drop a patch no longer needed
- fix-coredump-if-enable-systemd.patch: drop a patch no longer needed
- coreutils-i18n.patch: update

* Thu Jul 11 2024 jchzhou <zhoujiacheng@iscas.ac.cn> - 9.4-7
- add a workaround for fixing clang building issues

* Mon Jun 24 2024 huyubiao <huyubiao@huawei.com> - 9.4-6
- fix coredump if enable systemd

* Wed May 22 2024 liuh <liuhuan01@kylinos.cn> - 9.4-5
- backport patches from community

* Sat May 11 2024 liuh <liuhuan01@kylinos.cn> - 9.4-4
- sort: don’t trust st_size on /proc files

* Wed Mar 27 2024 Wenlong Zhang <zhangwenlong@loongson.cn> - 9.4-3
- delete strace for loongarch64

* Fri Feb 2 2024 jiangchuangang <jiangchuangang@huawei.com> - 9.4-2
- skip testcase for overlay filesystem because of no inotify_add_watch
  add test-skip-overlay-filesystem-because-of-no-inotify_add_watch.patch 

* Thu Feb 1 2024 lvgenggeng <lvgenggeng@uniontech.com> - 9.4-1
- bump to 9.4

* Wed Jan 24 2024 Jiangchuangang <jiangchuangang@huawei.com> - 9.3-3
- fix CVE-2024-0684

* Tue Aug 8 2023 Funda Wang <fundawang@yeah.net> - 9.3-2
- Add alias for sm3sum

* Wed Jul 12 2023 jiangchuangang<jiangchuangang@huawei.com> - 9.3-1
- update to 9.3

* Thu Jun 15 2023 jiangchuangang<jiangchuangang@huawei.com> - 9.0-10
- sync patches from community
- add backport-pr-fix-infinite-loop-when-double-spacing.patch
  backport-wc-ensure-we-update-file-offset.patch

* Fri Mar 17 2023 jiangchuangang<jiangchuangang@huawei.com> - 9.0-9
- add backport-fts-fail-gracefully-when-out-of-memory.patch for malloc failed

* Fri Mar 17 2023 jiangchuangang<jiangchuangang@huawei.com> - 9.0-8
- sync patches from community
- add backport-comm-fix-NUL-output-delimiter-with-total.patch
  backport-stty-validate-ispeed-and-ospeed-arguments.patch
  backport-fts-fix-race-mishandling-of-fstatat-failure.patch
  backport-stty-fix-off-by-onecolumn-wrapping-on-output.patch
  backport-copy-copy_file_range-handle-ENOENT-for-CIFS.patch
  backport-tail-fix-support-for-F-with-non-seekable-files.patch 

* Wed Oct 19 2022 wuzx<wuzx1226@qq.com> - 9.0-7
- add sw64 patch

* Sat Aug 27 2022 zoulin <zoulin13@h-partners.com> - 9.0-6
- fix 'sort -g' don't meet expectations

* Thu Jul 21 2022 xueyamao <xueyamao@kylinos.cn> - 9.0-5
- a new option df --direct

* Thu Jul 7 2022 zoulin <zoulin13@h-partners.com> - 9.0-4
- Auto display color difference when use ls

* Tue Mar 29 2022 panxiaohe <panxh.life@foxmail.com> - 9.0-3
- dd: improve integer overflow checking
- dd: do not access uninitialized
- df: fix memory leak
- ls: avoid triggering automounts
- stat: only automount with --cached=never

* Sat Feb 12 2022 yangzhuangzhuang <yangzhuangzhuang1@h-partners.com> - 9.0-2
- timeout: ensure --foreground -k exits with status 137

* Tue Jan 18 2022 wangchen <wangchen137@huawei.com> - 9.0-1
- Update to 9.0

* Tue Jul 20 2021 wangchen <wangchen137@huawei.com> - 8.32-7
- Delete unnecessary gdb from BuildRequires

* Mon Jul 5 2021 yangzhuangzhuang <yangzhuangzhuang1@huawei.com> - 8.32-6
- Add Buildrequires tcl to prevent "the /usr/bin/env: 'tclsh': No such file or direcory" error during compilation.

* Mon May 31 2021 panxiaohe <panxiaohe@huawei.com> - 8.32-5
- make mknod work again in chroot without /proc being mounted

* Wed Mar 24 2021 zoulin <zoulin13@huawei.com> - 8.32-4
- add -fsign-char for aarch64 for test-localeconv

* Mon Jan 11 2021 wangchen <wangchen137@huawei.com> - 8.32-3
- backport patches from upstream

* Wed Aug 26 2020 chenbo pan <panchenbo@uniontech.com> - 8.32-2
- fix patch error

* Wed Jul 29 2020 Liquor <lirui130@hauwei.com> - 8.32-1
- update to 8.32

* Thu Apr 30 2020 openEuler Buildteam <buildteam@openeuler.org> - 8.31-5
- Judge if selinux is enabled for the tests that requires selinux

* Sat Mar 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 8.31-4
- Add build requires of gdb

* Thu Feb 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 8.31-3
- Enable check and uname -p/-i as well as df --direct

* Fri Jan 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 8.31-2
- Strengthen patch

* Thu Jan 9 2020 openEuler Buildteam <buildteam@openeuler.org> - 8.31-1
- Update version to 8.31-1

* Wed Dec 25 2019 openEuler Buildteam <buildteam@openeuler.org> - 8.30-8
- Revert last commit

* Thu Dec 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 8.30-7
- delete unneeded patch

* Wed Nov 6 2019 openEuler Buildteam <buildteam@openeuler.org> - 8.30-6
- delete unneeded comments

* Thu Aug 29 2019 hexiaowen <hexiaowen@huawei.com> - 8.30-5
- Package rebuild

* Wed Aug 21 2019 gaoyi <gaoyi15@huawei.com> - 8.30-4.h8
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: remove patches' prefix starting with backport

* Wed May 08 2019 gulining<gulining1@huawei.com> - 8.30-4.h7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:revert patch for rtos

* Wed May 8 2019 liusirui<liusirui@huawei.com> - 8.30-4.h6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:rename some patches

* Sat Apr 6 2019 luochunsheng<luochunsheng@huawei.com> - 8.30-4.h5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:remove sensitive information

* Fri Mar 15 2019 yanghua <yanghua21@huawei.com> - 8.30-4.h4
- Type:bugfix
- ID:NA
- SUG:restart
- DESC:echo always process escapes when POSIXLY_CORRECT is
       sync fix open fallback bug
       tail fix handling of broken pipes with SIGPIPE ignor
       seq output decimal points consistently with invalid

* Thu Feb 14 2019 zoujing <zoujing13@huawei.com> - 8.30-4.h3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:failed to exec scriptlet interpreter

* Wed Jan 23 2019 sunguoshuai <sunguoshuai@huawei.com> - 8.30-4.h2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:sync 

* Fri Dec 28 2018 hushiyuan <hushiyuan@huawei.com> - 8.30-4.h1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add provides to coreutils-single to make it a drop-in replacement (#1572693)
- reintroduce very old Provides (mktemp, sh-utils, textwrap, fileutils, stat)

* Thu Jul 12 2018 hexiaowen <hexiaowen@huawei.com> - 8.30-1
- Pacakge init
